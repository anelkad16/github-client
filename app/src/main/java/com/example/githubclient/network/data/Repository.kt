package com.example.githubclient.network.data

import com.google.gson.annotations.SerializedName

data class Repository(
    @SerializedName(value = "name") val name: String,
    @SerializedName(value = "description") val description: String?,
    @SerializedName(value = "forks_count") val forks: Int,
    @SerializedName(value = "watchers_count") val watchers: Int,
    @SerializedName(value = "open_issues_count") val openIssues: Int,
    @SerializedName(value = "parent") val parentRepository: Repository?
)