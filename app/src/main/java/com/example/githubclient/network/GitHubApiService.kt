package com.example.githubclient.network

import android.util.Log
import com.example.githubclient.network.data.Repository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private const val BASE_URL = "https://api.github.com/"

private val loggingInterceptor = HttpLoggingInterceptor { message ->
    Log.d("OkHttp", message)
}
    .apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

private val okHttpClient = OkHttpClient
    .Builder()
    .addInterceptor(loggingInterceptor)
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .client(okHttpClient)
    .baseUrl(BASE_URL)
    .build()

interface GitHubApiService {
    @GET("orgs/{org}/repos")
    suspend fun getRepositoryList(@Path("org") organization: String): List<Repository>

    @GET("repos/{org}/{repo}")
    suspend fun getRepositoryDetails(
        @Path("org") organization: String,
        @Path("repo") repository: String,
    ): Repository
}

object GitHubApi {
    val retrofitService: GitHubApiService by lazy { retrofit.create(GitHubApiService::class.java) }
}