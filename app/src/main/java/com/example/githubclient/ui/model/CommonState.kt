package com.example.githubclient.ui.model

sealed class CommonState {
    data object ShowLoading : CommonState()
    data object HideLoading : CommonState()
    data class Error(val error: String) : CommonState()
    data class Result<out T : Any>(val result: T) : CommonState()
}