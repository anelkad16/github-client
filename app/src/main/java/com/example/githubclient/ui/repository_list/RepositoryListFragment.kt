package com.example.githubclient.ui.repository_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.githubclient.R
import com.example.githubclient.databinding.FragmentRepositoryListBinding
import com.example.githubclient.network.data.Repository
import com.example.githubclient.ui.model.CommonState
import com.example.githubclient.ui.repository_details.RepositoryDetailsFragment
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class RepositoryListFragment : Fragment() {

    private val viewModel: RepositoryListViewModel by viewModels()

    private val adapter = RepositoryListAdapter(
        onRepositoryClick = ::onClickRepository
    )

    private var _binding: FragmentRepositoryListBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRepositoryListBinding.inflate(inflater)
        binding.rvList.adapter = adapter
        binding.searchButton.setOnClickListener {
            viewModel.searchRepositories(binding.textInput.text.toString().trim())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.onEach { state ->
            when (state) {
                is CommonState.Error -> {
                    showErrorState(state.error)
                }

                is CommonState.HideLoading -> {
                    binding.progressBar.isVisible = false
                }

                is CommonState.ShowLoading -> {
                    showLoadingState()
                }

                is CommonState.Result<*> -> {
                    showContentState()
                    adapter.updateRepositories(state.result as? List<Repository>)
                }
            }
        }.launchIn(lifecycleScope)
    }

    private fun onClickRepository(repository: Repository) {
        val detailsFragment = RepositoryDetailsFragment.newInstance(
            organization = binding.textInput.text.toString().trim(),
            repo = repository.name
        )
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, detailsFragment)
            .addToBackStack(null)
            .commit()
    }

    private fun showLoadingState() {
        binding.progressBar.isVisible = true
        binding.rvList.isVisible = false
    }

    private fun showContentState() {
        binding.rvList.isVisible = true
    }

    private fun showErrorState(message: String) {
        binding.rvList.isVisible = false
        Toast
            .makeText(
                requireContext(),
                message,
                Toast.LENGTH_SHORT
            )
            .show()
    }
}