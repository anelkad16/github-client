package com.example.githubclient.ui.repository_list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.databinding.FragmentItemBinding
import com.example.githubclient.network.data.Repository

class RepositoryListAdapter(
    private val onRepositoryClick: (Repository) -> Unit,
    private val values: MutableList<Repository> = mutableListOf()
) : RecyclerView.Adapter<RepositoryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            FragmentItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.bind(item)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateRepositories(repositoryList: List<Repository>?) {
        if (repositoryList == null) return
        values.clear()
        values.addAll(repositoryList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(private val binding: FragmentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

            init {
                binding.root.setOnClickListener {
                    onRepositoryClick.invoke(values[position])
                }
            }

        fun bind(item: Repository) = with(binding) {
            itemName.text = item.name
            itemDescription.text = if (item.description.isNullOrBlank()) "No description message" else item.description
        }
    }
}