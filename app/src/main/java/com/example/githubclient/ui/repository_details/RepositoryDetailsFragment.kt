package com.example.githubclient.ui.repository_details

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.githubclient.databinding.FragmentRepositoryDetailsBinding
import com.example.githubclient.network.data.Repository
import com.example.githubclient.ui.model.CommonState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

private const val ORGANIZATION = "ORGANIZATION"
private const val REPO = "REPO"

class RepositoryDetailsFragment : Fragment() {

    private val viewModel: RepositoryDetailsViewModel by viewModels()
    private lateinit var _binding: FragmentRepositoryDetailsBinding
    private val binding
        get() = _binding

    companion object {
        @JvmStatic
        fun newInstance(organization: String, repo: String) =
            RepositoryDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(ORGANIZATION, organization)
                    putString(REPO, repo)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val organization = it.getString(ORGANIZATION)
            val repoName = it.getString(REPO)
            viewModel.selectRepository(organization, repoName)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRepositoryDetailsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state.onEach { state ->
            when (state) {
                is CommonState.Error -> {
                    showErrorState(state.error)
                }

                is CommonState.HideLoading -> {
                    binding.progressBar.isVisible = false
                }

                is CommonState.ShowLoading -> {
                    showLoadingState()
                }

                is CommonState.Result<*> -> {
                    showContentState(state.result as Repository)
                }
            }
        }.launchIn(lifecycleScope)
    }

    @SuppressLint("SetTextI18n")
    private fun showContentState(repository: Repository) = with(binding) {
        itemName.text = "Name: ${repository.name}"
        itemDescription.text = "Description: ${if (repository.description.isNullOrBlank()) "No description message" else repository.description}"
        forks.text = "Forks: ${repository.forks}"
        watchers.text = "Watchers: ${repository.watchers}"
        openIssues.text = "Open issues: ${repository.openIssues}"
        parentRepoName.text = "Parent name: ${repository.parentRepository?.name}"
    }

    private fun showLoadingState() {
        binding.progressBar.isVisible = true
    }

    private fun showErrorState(message: String) {
        Toast
            .makeText(
                requireContext(),
                message,
                Toast.LENGTH_SHORT
            )
            .show()
    }
}