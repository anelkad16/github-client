package com.example.githubclient.ui.repository_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.network.GitHubApi
import com.example.githubclient.network.GitHubApiService
import com.example.githubclient.ui.model.CommonState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

private const val LOAD_FAIL = "Failed to load repositories"

class RepositoryDetailsViewModel : ViewModel() {
    private val gitHubService: GitHubApiService = GitHubApi.retrofitService

    private var _state = MutableStateFlow<CommonState>(CommonState.HideLoading)
    val state: StateFlow<CommonState> = _state

    fun selectRepository(organization: String?, repoName: String?) {
        if (organization == null || repoName == null) return
        viewModelScope.launch {
            _state.value = CommonState.ShowLoading
            try {
                val repositoryDetails = gitHubService.getRepositoryDetails(organization, repoName)
                _state.value = CommonState.Result(repositoryDetails)
            } catch (exp: Throwable) {
                _state.value = CommonState.Error(LOAD_FAIL)
            }
            _state.value = CommonState.HideLoading
        }
    }
}