package com.example.githubclient.ui.repository_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.network.GitHubApi
import com.example.githubclient.network.GitHubApiService
import com.example.githubclient.ui.model.CommonState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

private const val NO_REPOS_FOUND_MSG = "No repos found for this organization"
private const val LOAD_FAIL = "Failed to load repositories"

class RepositoryListViewModel : ViewModel() {
    private val gitHubService: GitHubApiService = GitHubApi.retrofitService

    private var _state = MutableStateFlow<CommonState>(CommonState.HideLoading)
    val state: StateFlow<CommonState> = _state

    fun searchRepositories(organization: String) {
        viewModelScope.launch {
            _state.value = CommonState.ShowLoading
            try {
                val repositoryList = gitHubService.getRepositoryList(organization)
                _state.value = CommonState.Result(repositoryList)
                if (repositoryList.isEmpty()) {
                    _state.value = CommonState.Error(NO_REPOS_FOUND_MSG)
                }
                _state.value = CommonState.HideLoading
            } catch (exp: Throwable) {
                _state.value = CommonState.Error(LOAD_FAIL)
            }
            _state.value = CommonState.HideLoading
        }
    }
}